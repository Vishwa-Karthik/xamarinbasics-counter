﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Counter
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        int count = 0;
        private void Button_Clicked(object sender, EventArgs e)
        {
            count++;
            ((Button)sender).Text = $"You clicked me {count} times";
        }
        private void Btn2(object sender, EventArgs e)
        {
            ((Button)sender).Text = "You clicked 2nd Button";
        }

        private void FNameChanged(object sender, TextChangedEventArgs e)
        {
            String oldText = e.OldTextValue;
            String newText = e.NewTextValue;
            Console.Write(oldText);
            Console.WriteLine(newText);

        }

        private void Entry_Completed(object sender, EventArgs e)
        {
            String val = ((Entry)sender).Text;
        }
    }
}
