# Xamarin Basics

### Simple Counter application using C#

<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-counter/-/raw/master/counter1.jpg" width="200" height="400" >
&emsp;
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-counter/-/raw/master/counter2.jpg" width="200" height="400" >
</p>
